<?php
$media = filter_input(INPUT_GET,'launch');
$stop = filter_input(INPUT_GET,'stop');
$playing = false;

$files = [];
$selectedFile = null;
$it = new RecursiveDirectoryIterator("/media/externe/film");
$display = Array ( 'mkv', 'mp4','avi' );
foreach(new RecursiveIteratorIterator($it) as $file)
{
    @$ext = strtolower(array_pop(explode('.', $file)));
    if (in_array($ext, $display)){
      $date = new DateTime();
      $date->setTimestamp(filectime($file));
      $files[] = ['file'=>$file,'date'=>$date];
      if ($media !== null && $media == $file){
        $selectedFile = $media;
      }
    }
}


if ($stop !== null){
	exec('killall vlc');
}
if ($selectedFile !== null){
    $playing = true;
    exec("export DISPLAY=:0.0;/usr/bin/cvlc ".str_replace(' ','\\ ',$selectedFile).' --fullscreen > /dev/null 2>&1 &');
}


?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Landing Page - Start Bootstrap Theme</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="css/landing-page.min.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation 
    <nav class="navbar navbar-light bg-light static-top">
      <div class="container">
        <a class="navbar-brand" href="#">Start Bootstrap</a>
        <a class="btn btn-primary" href="#">Sign In</a>
      </div>
    </nav>-->

    <!-- Masthead -->
    <header class="text-center">
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <h1 class="mb-5">playzone</h1>
          </div>
        </div>
      </div>
    </header>
    
    <?php
      if ($playing){
        echo '<section class="fbg-light">
      <div class="container">
      <div class="row justify-content-md-center">';
        
        echo '<div class="alert alert-danger col-md-auto col text-center" role="alert">';
        echo '<h6>playing '.@array_pop(explode('/', $media)) .'</h6>';
        echo '<a  class="btn btn-dark btn-lg" href="?stop=1">stop it</a>';
        echo '</div>';
        echo'</div></div></section>';
      }
    ?>

    <!-- Icons Grid -->
    <section class="fbg-light">
      <div class="container">
        <div class="">
          <h2>Médias</h2>
          <table id="myTable">  
            <thead>
              <td>nom</td>
              <td>date</td>
              </thead>
              <tbody>
          <?php
          foreach($files as $file)
          {
                  echo '<tr>';
                  echo '<td><a href="?launch='.$file['file'].'">'.@array_pop(explode('/', $file['file'])) .'</a> </td>';
                  echo '<td>'.$file['date']->format('d m Y').' </td>';
                  echo '</tr>';
                  
          }          ?>
          </tbody>
          </table>
          </div>
    </section>


    

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script>
    $(document).ready( function () {
      $('#myTable').DataTable({
        "order": [[ 1, "desc" ]]
      });
      } );
      </script>
  </body>

</html>
